package models

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestTimeBeefModels(t *testing.T) {
	var firstRun time.Duration

	testcases := []struct {
		name          string
		meat          string
		checkResponse func(t *testing.T, data map[string]uint32, err error, duration time.Duration)
	}{
		{
			name: "FIRST RUN NO CHACHE",
			meat: "beef",
			checkResponse: func(t *testing.T, data map[string]uint32, err error, duration time.Duration) {
				require.NoError(t, err)
				require.NotNil(t, data)

				firstRun = duration
			},
		},
		{
			name: "SECOND RUN SHOULD FASTER",
			meat: "beef",
			checkResponse: func(t *testing.T, data map[string]uint32, err error, duration time.Duration) {
				require.NoError(t, err)
				require.NotNil(t, data)
				require.Greater(t, firstRun, duration)
			},
		},
		{
			name: "NEXT RUN SHOULD FASTER",
			meat: "beef",
			checkResponse: func(t *testing.T, data map[string]uint32, err error, duration time.Duration) {
				require.NoError(t, err)
				require.NotNil(t, data)
				require.Greater(t, firstRun, duration)
			},
		},
		{
			name: "INVALID MEAT URL",
			meat: "pork",
			checkResponse: func(t *testing.T, data map[string]uint32, err error, duration time.Duration) {
				require.Error(t, err)
				require.Nil(t, data)
				require.NotZero(t, duration)
			},
		},
	}

	for i := range testcases {
		tc := testcases[i]

		t.Run(tc.name, func(t *testing.T) {
			start := time.Now()
			data, err := GetMeatFromCache(tc.meat)
			duration := time.Since(start)
			tc.checkResponse(t, data, err, duration)
		})
	}
}
