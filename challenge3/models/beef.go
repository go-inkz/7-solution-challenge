package models

import (
	"errors"
	"io"
	"log"
	"net/http"
	"regexp"
	"sync"
)

var (
	meatCache = make(map[string]map[string]uint32)
	storeURL  = map[string]string{
		"beefURL": "https://baconipsum.com/api/?type=meat-and-filler&paras=99&format=text",
	}
	cacheMutex sync.RWMutex
)

func GetMeatFromCache(protein string) (map[string]uint32, error) {
	cacheMutex.RLock()
	cachedData, ok := meatCache[protein]
	cacheMutex.RUnlock()
	if ok {
		return cachedData, nil
	}

	url := protein + "URL"
	beef, err := GetMeat(storeURL[url])
	if err != nil {
		return nil, err
	}

	cacheMutex.Lock()
	meatCache[protein] = beef
	cacheMutex.Unlock()

	return beef, nil
}

func GetMeat(url string) (map[string]uint32, error) {
	if url == "" {
		return nil, errors.New("empty URL provided")
	}
	// Make get request
	resp, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	data := string(body)
	regx := regexp.MustCompile(`\b[\w-]+\b`)
	matches := regx.FindAllString(data, -1)

	// Add to hashmap
	beef := make(map[string]uint32)
	for _, menu := range matches {
		lowerCaseMenu := strings.ToLower(menu)
		beef[lowerCaseMenu]++
	}
	return beef, nil
}
