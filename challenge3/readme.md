# Challenge3

## Run server with

You should now on `Challenge3` directory

1. GNU 
```bash
make server
```
2. vanilla Go
```go
go run ./cmd/main.go
```
3. air 
```bash
air
```

## Testing
```bash
make test
```


## Testing gRPC with evans
open new terminal
```bash
make evans
```
- in `evans`
go to package
```evans
show package
package meat
```
use MeatCounter service
```evans
show service
service MeatCounter
```
call CountMeats method
```evans
call CountMeats
```
```evans
exit
```


## Perfomance
issue: fetch beef api are slow

solution: 
* `concurrency`: http handlefunc are already go-routein + only have 1 api => no need to use go-routein
* `chaching`: data not often changing => suit more for this challenge => careful read write in same time => use mutex
