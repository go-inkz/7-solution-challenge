package gapi

import "pieFireDire/proto"

type Server struct {
	proto.UnimplementedMeatCounterServer
}

func NewServer() (*Server, error) {
	server := &Server{}
	return server, nil
}
