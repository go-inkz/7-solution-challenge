package gapi

import (
	"context"
	"pieFireDire/proto"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestBeefAPI(t *testing.T) {
	testCases := []struct {
		name          string
		checkResponse func(t *testing.T, res *proto.BeefResponse, err error)
	}{
		{
			name: "OK",
			checkResponse: func(t *testing.T, res *proto.BeefResponse, err error) {
				require.NoError(t, err)
				require.NotNil(t, res)
			},
		},
	}

	for i := range testCases {
		tc := testCases[i]

		t.Run(tc.name, func(t *testing.T) {
			server := newTestServer(t)
			res, err := server.CountMeats(context.Background(), &proto.BeefRequest{})
			tc.checkResponse(t, res, err)
		})
	}

}
