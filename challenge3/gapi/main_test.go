package gapi

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func newTestServer(t *testing.T) *Server {
	server, err := NewServer()
	require.NoError(t, err)
	return server
}
