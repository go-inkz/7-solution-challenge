package gapi

import (
	"context"
	"pieFireDire/models"
	"pieFireDire/proto"
)

func (Server *Server) CountMeats(ctx context.Context, req *proto.BeefRequest) (*proto.BeefResponse, error) {
	beef, err := models.GetMeatFromCache("beef")
	if err != nil {
		return nil, err
	}

	rsp := &proto.BeefResponse{
		MeatCounts: beef,
	}
	return rsp, nil
}
