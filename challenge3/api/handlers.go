package api

import (
	"net/http"
	"pieFireDire/models"

	"github.com/gin-gonic/gin"
)

func (server *Server) beefHandlers(ctx *gin.Context) {
	beef, err := models.GetMeatFromCache("beef")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err)
	}

	// jsonData, err := json.Marshal(map[string]interface{}{"beef": beef})
	// if err != nil {
	// 	ctx.JSON(http.StatusInternalServerError, gin.H{
	// 		"error": err.Error(),
	// 	})
	// 	return
	// }
	// ctx.Data(http.StatusOK, "application/json", jsonData)

	ctx.JSON(http.StatusOK, gin.H{"beef": beef})
}
