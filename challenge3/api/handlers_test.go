package api

import (
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestMeatAPI(t *testing.T) {
	testCases := []struct {
		name          string
		endPoint      string
		checkResponse func(t *testing.T, recorder *httptest.ResponseRecorder)
	}{
		{
			name:     "OK",
			endPoint: "/beef/summary",
			checkResponse: func(t *testing.T, recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusOK, recorder.Code)

				res := recorder.Result()
				body, err := io.ReadAll(recorder.Body)
				require.NoError(t, err)
				res.Body.Close()
				require.NotNil(t, body)
			},
		},
		{
			name:     "WRONG END POINT",
			endPoint: "/pork/summary",
			checkResponse: func(t *testing.T, recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusNotFound, recorder.Code)
			},
		},
	}

	for i := range testCases {
		tc := testCases[i]

		t.Run(tc.name, func(t *testing.T) {
			server := newTestServer(t)
			recoder := httptest.NewRecorder()
			req, err := http.NewRequest(http.MethodGet, tc.endPoint, nil)
			require.NoError(t, err)
			server.router.ServeHTTP(recoder, req)
			tc.checkResponse(t, recoder)
		})
	}

}
