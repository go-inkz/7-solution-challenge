package main

import (
	"fmt"
	"log"
	"net"
	"pieFireDire/api"
	"pieFireDire/gapi"
	"pieFireDire/proto"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const (
	JSON_PORT = 3000
	GRPC_PORT = 50051
)

func main() {
	go runJSONRestful()
	run_gRPC()
}

func runJSONRestful() {
	server, err := api.NewServer()
	if err != nil {
		log.Fatalln("Unable to Create Restful server: ", err)
	}
	server.Start(fmt.Sprintf(":%d", JSON_PORT))
	log.Println("RESTFUL JSON API start at PORT :", JSON_PORT)
}

func run_gRPC() {
	server, err := gapi.NewServer()
	if err != nil {
		log.Fatalln("Unable to Create GRPC server: ", err)
	}

	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)
	proto.RegisterMeatCounterServer(grpcServer, server)
	reflection.Register(grpcServer)

	listener, err := net.Listen("tcp", fmt.Sprintf("localhost:%d", GRPC_PORT))
	if err != nil {
		log.Fatalln("Cannot create listener")
	}
	defer listener.Close()

	log.Println("Start gRPC server ")
	err = grpcServer.Serve(listener)
	if err != nil {
		log.Println("Cannot start gRPC server ")
	}
}
